Images = new FS.Collection("images", {
  stores: [
    new FS.Store.GridFS("images", {}),
		//new FS.Store.FileSystem("thumbs", { transformWrite: createSquareThumb })
  ],
  filter: {
		maxSize: 768000,
    allow: {
      contentTypes: ['image/*'] //allow only images in this FS.Collection
    }
  }
});

Images.allow({
  insert: function (userId, doc) {
  	return true;
  },
  download: function(userId){
    return true;
	},
  update: function(userId){
    return true;
	}
})

if(Meteor.isServer){
	Meteor.publish('images', function () {
		return Images.find();
	});
}
