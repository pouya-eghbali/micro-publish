
UserProfileSchema = new SimpleSchema({
	name: {
		type: String,
		optional: true,
		label: 'نام'
	},
	about: {
		type: String,
		optional: true,
		label: 'درباره'
	},
	slogan: {
		type: String,
		optional: true,
		label: 'عنوان'
	},
	banner: {
    type: String,
    optional: true,
		label: 'پس زمینه پروفایل',
    autoform:{
      afFieldInput:{
        type: 'fileUpload',
        collection: 'images',
        label: 'انتخاب تصویر'
			}
		}
	},
  avatar: {
    type: String,
    optional: true,
		label: 'تصویر شخصی',
    autoform:{
      afFieldInput:{
        type: 'fileUpload',
        collection: 'images',
        label: 'انتخاب تصویر'
			}
		}
	},
	website: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'وبسایت'
	},
  facebook: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'فیسبوک'
	},
  twitter: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'توییتر'
	},
  linkedin: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'لینکداین'
	},
  googleplus: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'گوگل پلاس'
	},
  github: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'گیت هاب'
	},
	bitbucket: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'بیت باکت'
	},
	youtube: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'یوتیوب'
	},
	vimeo: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'ویمئو'
	},
	aparat: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
		label: 'آپارات'
	},
});

UserSchema = new SimpleSchema({
	username: {
		type: String,
		regEx: /^[a-z0-9A-Z_]{3,15}$/,
		optional: true
	},
	emails: {
		type: [Object],
		max: 1,
	},
	"emails.$.address": {
		type: String,
		label: 'ایمیل',
		regEx: SimpleSchema.RegEx.Email
	},
	"emails.$.verified": {
		type: Boolean
	},
	createdAt: {
		type: Date
	},
	profile: {
		type: UserProfileSchema,
		optional: true
	},
	services: {
		type: Object,
		optional: true,
		blackbox: true
	}
});
Meteor.users.attachSchema(UserSchema);
Meteor.users.allow({
	  insert: function () { return true; },
	  update: function (userId, doc) {
			if(userId == doc.userId) {
				return true;
			}
			return false;
		},
	  remove: function (userId, doc) {return false;}
});
if(Meteor.isServer){
	Meteor.publish("userData", function (username) {
		if(!username){
    	return Meteor.users.find({'_id': this.userId}, {fields: {profile: 1}});
		} else {
			return Meteor.users.find({'username': username}, {fields: {'username': 1, profile: 1}});
		}
	});
} else {
	Handlebars.registerHelper('userData', function() {
		if(!FlowRouter.getParam('username')){
    	return Meteor.users.findOne({'_id': Meteor.userId()})
		} else {
			return Meteor.users.findOne({'username': FlowRouter.getParam('username')});
		}
  });
	Handlebars.registerHelper('adminData', function() {
			return Meteor.users.findOne({'username': 'admin'});
  });
}
