if (typeof Handlebars !== 'undefined') {
  Handlebars.registerHelper('main_script', function(name, options) {
    $('body').append('<script src="/assets/js/jquery.min.js"></script>');
    $('body').append('<script src="/assets/js/snap.svg-min.js"></script>');
    $('body').append('<script src="/assets/js/jquery.poptrox.min.js"></script>');
    $('body').append('<script src="/assets/js/skel.min.js"></script>');
    $('body').append('<script src="/assets/js/util.js"></script>');
    $('body').append('<!--[if lte IE 8]><script src="/assets/js/ie/respond.min.js"></script><![endif]-->');

    /* Medium */

    $('body').append('<script src="/assets/js/medium/js/medium-editor.js"></script>');
    $('body').append('<script src="/assets/js/handlebars.runtime.js"></script>');
    $('body').append('<script src="/assets/js/jquery-sortable/source/js/jquery-sortable.js"></script>');
    $('body').append('<script src="/assets/js/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>');
    $('body').append('<script src="/assets/js/jquery-file-upload/js/jquery.iframe-transport.js"></script>');
    $('body').append('<script src="/assets/js/jquery-file-upload/js/jquery.fileupload.js"></script>');
    $('body').append('<script src="/assets/js/medium-editor-insert-plugin/dist/js/medium-editor-insert-plugin.min.js"></script>');

    /* Main Script */

    $('body').append('<script src="/assets/js/main.js"></script>');

  });

  Handlebars.registerHelper('isSubReady', function(sub) {
    if(sub) {
      return FlowRouter.subsReady(sub);
    } else {
      return FlowRouter.subsReady();
    }
  });

  Handlebars.registerHelper('SummarizeAndTrim', function(body) {
    var s = ''; $('<div>').html(body).children().slice(0,5).each(function(){ s += $(this)[0].outerHTML }); s += '...';
    return s.replace(/(<br>|<p>\s*<\/p>)/gi, '').replace(/(<\/p>)\.\.\./gi, '\.\.\.<\/p>');
  });

}
