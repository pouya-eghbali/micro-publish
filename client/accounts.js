Template.signup_form.events({
  "click .signup": function(event, template) {
    event.preventDefault();
    Accounts.createUser({
      username: template.find("#signup-username").value,
      email: template.find("#signup-email").value,
      password: template.find("#signup-password").value,
      profile: {
        name: template.find("#signup-name").value
        // Other required field values can go here
      }
    }, function(error) {
      if (error) {
        // Display the user creation error to the user however you want
        console.log(error)
      } else {
        FlowRouter.go('/')
      }
    });
  }
});

Template.signin_form.events({
  "click .signin": function(event, template) {
    event.preventDefault();
    Meteor.loginWithPassword(
      template.find("#signin-username").value,
      template.find("#signin-password").value,
      function(error) {
        if (error) {
          console.log(error)// Display the login error to the user however you want
        } else {
          FlowRouter.go('/')
        }
      }
    );
  }
});

Template.front.events({
  "click .signout": function(event, template) {
    event.preventDefault();
    Meteor.logout(function(error) {
      if (error) {
        console.log(error)// Display the logout error to the user however you want
      }
    });
  }
});
